package ua.danit.framework.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import ua.danit.framework.invocations.Parameter;


public class Reflections {

  /**
   * getMethods(Class clazz).
   * This method takes from the class all the methods that are in it,
   * through reflection API
   */
  public static List<Method> getMethods(Class clazz) {
    if (clazz == null) {
      return null;
    }
    return Arrays.asList(clazz.getMethods());
  }

  /**
   * getAnnotation(Method method, Class anno).
   * This method takes from the class each methods and take all annotation that are in it,
   * through reflection API
   */

  public static Annotation getAnnotation(Method method, Class anno) {
    if (method == null) {
      return null;
    }
    return method.getAnnotation(anno);
  }

  /**
   * getParameters(Method getMethod).
   * This method taken from method all parameters through reflection API
   */

  public static List<Parameter> getParameters(Method getMethod) {
    return null;
  }
}
